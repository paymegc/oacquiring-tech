<?php

class API_oacquiring_tech
{
    private $URL_PRO            = "https://api.oacquiring.com/txn";

    private $URL                = "";
    private $merchantId         = "";
    private $terminalId         = "";
    private $signKey            = "";
    private $secret             = "";
    private $user               = "";
    private $password           = "";

    private $URL_DEV            = "https://testapi.oacquiring.com/txn";
    private $merchantId_DEV     = "72f0c567-4c8f-4724-b083-b4c508bf7a7e";
    private $terminalId_DEV     = "7df5987d-c01b-4a49-883e-25d25ed84af5";
    private $signKey_DEV        = "5LX5xMkw";
    private $secret_DEV         = "oLXPzN3VcYHX7Gy1LNNJ";
    private $user_DEV           = "test2@test.com";
    private $password_DEV       = "test123!";

    function __construct($atts = array())
    {
        $this->merchantId   = $atts["merchantId"];
        $this->terminalId   = $atts["terminalId"];
        $this->signKey      = $atts["signKey"];
        $this->secret       = $atts["secret"];
        $this->user         = $atts["user"];
        $this->password     = $atts["password"];
        $this->URL          = $this->URL_PRO;

        if($atts["testmode"]){
            $this->merchantId   = $this->merchantId_DEV;
            $this->terminalId   = $this->terminalId_DEV;
            $this->signKey      = $this->signKey_DEV;
            $this->secret       = $this->secret_DEV;
            $this->user         = $this->user_DEV;
            $this->password     = $this->password_DEV;
            $this->URL          = $this->URL_DEV;
        }
    }
    private function request($atts = array())
    {
        $curl = curl_init();
        $config =  array(
            CURLOPT_URL             => $this->URL.$atts["url"],
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_ENCODING        => '',
            CURLOPT_MAXREDIRS       => 10,
            CURLOPT_TIMEOUT         => 0,
            CURLOPT_FOLLOWLOCATION  => true,
            CURLOPT_HTTP_VERSION    => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST   => $atts["method"],
            CURLOPT_POSTFIELDS      => json_encode($atts["json"]),
            CURLOPT_HTTPHEADER      => array(
                'Content-Type: application/json',
                'Authorization: '.$this->TOKEN,
            ),
        );
        curl_setopt_array($curl,$config);

        $response = curl_exec($curl);

        curl_close($curl);
        $response = json_decode($response,true);

        $atts["header"] = array(
            'Content-Type: application/json',
            'Authorization '.$this->TOKEN,
        );
        addOATE_LOG(array(
            "type"      => "REQUEST",
            "config"    => $atts,
            "result"    => $response,
        ));

        return $response;
    }
    public function auth()
    {
        $result = $this->request(array(
            "method"    => "POST",
            "url"       => "/AuthToken",
            "json"      => array(
                "merchant_id"   => $this->merchantId,
                "secret"        => $this->secret,
            ),
        ));
        if($result["txn_status"] == "SUCCESS"){
            $this->TOKEN = $result["auth_token"];
        }
    }
    public function sale($json = [])
    {
        return $this->request(array(
            "method"    => "POST",
            "url"       => "/sale",
            "json"      => $json,
        ));
    }
    public function status()
    {
        return $this->request(array(
            "method"    => "POST",
            "url" => "/status",
            
        ));
    }
    public function refund($json = [])
    {
        return $this->request(array(
            "method"    => "POST",
            "url" => "/refund",
            "json"      => array_merge(
                array(
                    "merchant_id"   => $this->merchantId,
                    "terminal_id"   => $this->terminalId,
                ),
                $json,
            ),
        ));
    }
    public function checkout()
    {
        return $this->request(array(
            "method"    => "POST",
            "url" => "/checkout",
            
        ));
    }
}
