<?php

function OATE_get_optionPage($id = "OATE_optionPage")
{
    $OATE_optionPage = get_option($id);
    if($OATE_optionPage === false || $OATE_optionPage == null || $OATE_optionPage == ""){
        $OATE_optionPage = "[]";
    }
    $OATE_optionPage = json_decode($OATE_optionPage,true);
    return $OATE_optionPage;
}

function OATE_put_optionPage($id,$newItem)
{
    $OATE_optionPage = OATE_get_optionPage($id);
    $OATE_optionPage[] = array(
        "date" => date("c"),
        "data" => $newItem,
    );
    update_option($id,json_encode($OATE_optionPage));
}