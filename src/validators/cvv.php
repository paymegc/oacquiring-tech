<?php

function OATE_validateCVV($cvv,$type){
    $regex = "(\d{3})";
    $lenCVV = 3;
    if($type == "American Express"){
        $regex = "(\d{4})";
        $lenCVV = 4;
    }
    return preg_match($regex, $cvv) == 1 && strlen($cvv) == $lenCVV;
}