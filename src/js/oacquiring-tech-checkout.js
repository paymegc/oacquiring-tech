const log_OATE = (name, data, color = "white") => {
  if (OATE_LOG === true) {
    console.log(`%c [${name.toLocaleUpperCase()}]`, `color:${color};`, data);
  }
};

const showOATE_content = (id) => {
  const active = document.querySelector(".OATE_content.active");
  if (active) {
    active.classList.remove("active");
  }
  const select = document.getElementById(`OATE_content_${id}`);
  select.classList.add("active");
};

const OATE_addChangeDate = () => {
  const OATE_expiry_date = document.getElementById("OATE_expiry_date");
  const OATE_expiry_date_hidden = document.getElementById(
    "OATE_expiry_date_hidden"
  );
  if (OATE_expiry_date) {
    OATE_expiry_date_hidden.addEventListener("change", () => {
      OATE_expiry_date.value = OATE_expiry_date_hidden.value;
    });
  }
};
const OATE_addChangePaymentMode = () => {
  const OATE_payment_mode = document.getElementById("OATE_payment_mode");
  OATE_payment_mode.addEventListener("change", () => {
    log_OATE("Change OATE_payment_mode", OATE_payment_mode.value);
    showOATE_content(OATE_payment_mode.value);
  });
  showOATE_content(OATE_payment_mode.value);
};

const OATE_getTypeCard = (card) => {
  const visa = new RegExp("^4[0-9]{12}(?:[0-9]{3})?$");
  const mastercard = new RegExp("^5[1-5][0-9]{14}$");
  const mastercard2 = new RegExp("^2[2-7][0-9]{14}$");
  if (visa.test(card)) {
    return "VISA";
  }
  if (mastercard.test(card) || mastercard2.test(card)) {
    return "MASTERCARD";
  }
  return null;
};
const OATE_addChangeTypeCard = () => {
  const OATE_card_number = document.getElementById("OATE_card_number");
  const OATE_card_number_img = document.getElementById("OATE_card_number_img");
  OATE_card_number.addEventListener("input", () => {
    const typeCard = OATE_getTypeCard(OATE_card_number.value);
    if (typeCard) {
      OATE_card_number_img.classList.add("active");
      OATE_card_number_img.src = OATE_URL + "src/img/" + typeCard + ".png";
    } else {
      OATE_card_number_img.classList.remove("active");
    }
  });
};

const functionOacquiringTechCheckout = () => {
  OATE_addChangePaymentMode();
  OATE_addChangeDate();
  OATE_addChangeTypeCard();
};

const loadFunctionOacquiringTechCheckout = () => {
  jQuery(document.body).on("updated_checkout", function () {
    functionOacquiringTechCheckout();
  });
  functionOacquiringTechCheckout();
};

window.addEventListener("load", loadFunctionOacquiringTechCheckout);
