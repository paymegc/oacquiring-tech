<?php
/*
 * This action hook registers our PHP class as a WooCommerce payment gateway
 */
add_filter( 'woocommerce_payment_gateways', 'oacquiring_tech_add_gateway_class' );
function oacquiring_tech_add_gateway_class( $gateways ) {
	$gateways[] = 'WC_Oacquiring_Tech_Gateway'; // your class name is here
	return $gateways;
}

/*
 * The class itself, please note that it is inside plugins_loaded action hook
 */
add_action( 'plugins_loaded', 'oacquiring_tech_init_gateway_class' );
function oacquiring_tech_init_gateway_class() {

	class WC_Oacquiring_Tech_Gateway extends WC_Payment_Gateway {

 		/**
 		 * Class constructor, more about it in Step 3
 		 */
 		public function __construct() {

            $this->id = 'oacquiring_tech'; // payment gateway plugin ID
            $this->icon = ''; // URL of the icon that will be displayed on checkout page near your gateway name
            $this->has_fields = true; // in case you need a custom credit card form
            $this->method_title = 'Oacquiring Tech Gateway';
            $this->method_description = 'Payment for Wordpress use Oacquiring Tech'; // will be displayed on the options page

            $this->notification_url     = OATE_URL."src/routes/order/processing.php";
            $this->txn_request_url      = get_home_url();
        
            // gateways can support subscriptions, refunds, saved payment methods,
            // but in this tutorial we begin with simple payments
            $this->supports = array(
                'products'
            );
        
            // Method with all the options fields
            $this->init_form_fields();
        
            // Load the settings.
            $this->init_settings();
            $this->title = $this->get_option( 'title' );
            $this->description = $this->get_option( 'description' );
            $this->enabled = $this->get_option( 'enabled' );
            $this->testmode = 'yes' === $this->get_option( 'testmode' );
            $this->txn_request_ip = $this->get_option( 'txn_request_ip' );
        
            // This action hook saves the settings
            add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );

 		}
		/**
 		 * Plugin options, we deal with it in Step 3 too
 		 */
 		public function init_form_fields(){
            $this->form_fields = array(
                'enabled' => array(
                    'title'       => 'Enable/Disable',
                    'label'       => 'Enable Oacquiring Tech Gateway',
                    'type'        => 'checkbox',
                    'description' => '',
                    'default'     => 'no'
                ),
                'title' => array(
                    'title'       => 'Title',
                    'type'        => 'text',
                    'description' => 'This controls the title which the user sees during checkout.',
                    'default'     => 'Oacquiring Tech',
                    'desc_tip'    => true,
                ),
                'description' => array(
                    'title'       => 'Description',
                    'type'        => 'textarea',
                    'description' => 'This controls the description which the user sees during checkout.',
                    'default'     => 'Pay with your credit card via Oacquiring Tech',
                ),
                'configPayment' => array(
                    'title'       => 'Config Payment',
                    'type'        => 'tag',
                    'text'        => "Your config of payment, contact an advisor ",
                ),
                'testmode' => array(
                    'title'       => 'Test mode',
                    'label'       => 'Enable Test Mode',
                    'type'        => 'checkbox',
                    'description' => 'Place the payment gateway in test mode using test API keys.',
                    'default'     => 'yes',
                    'desc_tip'    => true,
                ),
                'merchantId' => array(
                    'title'       => 'MerchantId',
                    'type'        => 'text',
                ),
                'terminalId' => array(
                    'title'       => 'Terminal ID Visa (USD) ',
                    'type'        => 'text',
                ),
                'terminalIdMC' => array(
                    'title'       => 'Terminal ID MC (USD) ',
                    'type'        => 'text',
                ),
                'signKey' => array(
                    'title'       => 'SignKey',
                    'type'        => 'text',
                ),
                'secret' => array(
                    'title'       => 'Secret',
                    'type'        => 'password',
                ),
                'user' => array(
                    'title'       => 'User',
                    'type'        => 'text',
                ),
                'password' => array(
                    'title'       => 'Password',
                    'type'        => 'password',
                ),
                'whiteList' => array(
                    'title'       => 'White List',
                    'type'        => 'tag',
                    'text'        => "You must request support to add the following url and ip to the whitelist",
                ),
                'txn_request_url' => array(
                    'title'       => 'txn_request_url',
                    'type'        => 'info',
                    'text'        => $this->txn_request_url,
                ),
                'txn_request_ip' => array(
                    'title'       => 'txn_request_ip',
                    'type'        => 'text',
                ),
                'payment_mode' => array(
                    'title'       => 'Payment Mode',
                    'type'        => 'tag',
                    'text'        => "Your allowed payment methods",
                ),
                'payment_mode_credit_card' => array(
                  'title'   => __( 'Credit Card', 'woocommerce' ),
                  'label'   => __( 'Credit Card', 'woocommerce' ),
                  'type'    => 'checkbox',
                  'default' => 'yes',
                ),
                // 'payment_mode_wallet' => array(
                //   'title'   => __( 'Wallet', 'woocommerce' ),
                //   'label'   => __( 'Wallet', 'woocommerce' ),
                //   'type'    => 'checkbox',
                //   'default' => 'yes',
                // ),
                // 'payment_mode_BTC' => array(
                //   'title'   => __( 'BTC', 'woocommerce' ),
                //   'label'   => __( 'BTC', 'woocommerce' ),
                //   'type'    => 'checkbox',
                //   'default' => 'yes',
                // ),
            );
	 	}
         /**
          * Custon field tag
          */
        public function generate_tag_html( $key, $data ) { 
            $defaults = array(
                'class'             => '',
                'css'               => '',
                'custom_attributes' => array(),
                'desc_tip'          => false,
                'description'       => '',
                'title'             => '',
                'text'             => '',
            );
            $data = wp_parse_args( $data, $defaults );
            ob_start();
            ?>
            <tr valign="top" class="tag">
                <th scope="row" class="titledesc">
                    <label><?php echo wp_kses_post( $data['title'] ); ?> </label>
                </th>
                <td class="forminp">
                    <?php echo wp_kses_post( $data['text'] ); ?>
                </td>
            </tr>
            <style>
                .tag{
                    background: #1d2327;
                    color: white;
                    box-shadow: -50px 0 #1d2327,50px 0 #1d2327;
                }
                .tag label{
                    color: white !important;
                }
            </style>
            <?php
            return ob_get_clean();
        }
         /**
          * Custon field info
          */
        public function generate_info_html( $key, $data ) { 
            $defaults = array(
                'class'             => '',
                'css'               => '',
                'custom_attributes' => array(),
                'desc_tip'          => false,
                'description'       => '',
                'title'             => '',
                'text'             => '',
            );
            $data = wp_parse_args( $data, $defaults );
            ob_start();
            ?>
            <tr valign="top">
                <th scope="row" class="titledesc">
                    <label><?php echo wp_kses_post( $data['title'] ); ?> </label>
                </th>
                <td class="forminp">
                    <?php echo wp_kses_post( $data['text'] ); ?>
                </td>
            </tr>
            <?php
            return ob_get_clean();
        }

		/**
		 * You will need it if you want your custom credit card form, Step 4 is about it
		 */
		public function payment_fields() {
                // ok, let's display some description before the payment form
                if ( $this->description ) {
                    // you can instructions for test mode, I mean test card numbers etc.
                    if ( $this->testmode ) {
                        $this->description = ' TEST MODE ENABLED.';
                    }
                    // display the description with <p> tags etc.
                    echo wpautop( wp_kses_post( $this->description ) );
                }
                ?>
                <div class="form-row form-row-wide" hidden>
                    <label>
                        Payment Mode <span class="required">*</span>
                    </label>
                    <select name="OATE_payment_mode" id="OATE_payment_mode" style="width:100%">
                        <?php
                            if($this->get_option( 'payment_mode_credit_card' ) == "yes"){
                                ?>
                                <option value="cc">
                                    Credit card
                                </option>
                                <?php
                            }
                            if($this->get_option( 'payment_mode_wallet' ) == "yes"){
                                ?>
                                <option value="wallet">
                                    Wallet
                                </option>
                                <?php
                            }
                            if($this->get_option( 'payment_mode_BTC' ) == "yes"){
                                ?>
                                <option value="BTC">
                                    BTC
                                </option>
                                <?php
                            }
                        ?>
                    </select>
                </div>
                <?php
                if($this->get_option( 'payment_mode_credit_card' ) == "yes"){
                    ?>
                    <div id="OATE_content_cc" class="OATE_content">
                        <div class="form-row form-row-wide " >
                            <label>
                                Card Number <span class="required">*</span>
                            </label>
                            <div class="OATE_content_card_number">
                                <input id="OATE_card_number" name="OATE_card_number" type="text" autocomplete="off" required placeholder="Card Number">
                                <img src="" alt="" id="OATE_card_number_img" class="OATE_card_number_img">
                            </div>
                        </div>
                        <div class="form-row form-row-wide">
                            <label>
                                Name <span class="required">*</span>
                            </label>
                            <input id="OATE_name" name="OATE_name" type="text" autocomplete="off" required placeholder="Name">
                        </div>
                        <div class="form-row form-row-wide pos-r">
                            <label>
                                Expiry Date (YYYY-MM) <span class="required">*</span>
                            </label>
                            <input id="OATE_expiry_date" name="OATE_expiry_date" type="text" autocomplete="off" required placeholder="YYYY-MM">
                            <input id="OATE_expiry_date_hidden" name="OATE_expiry_date_hidden" type="month" autocomplete="off" required class="inputDateOver">
                        </div>
                        <div class="form-row form-row-wide">
                            <label>
                                CVV <span class="required">*</span>
                            </label>
                            <input id="OATE_cvv" name="OATE_cvv" type="number" autocomplete="off" required max="999" placeholder="XXX">
                        </div>
                        <div class="clear"></div>
                    </div>
                    <?php
                }
                if($this->get_option( 'payment_mode_wallet' ) == "yes"){
                    ?>
                    <div id="OATE_content_wallet" class="OATE_content">
                        TODO: Pendiente
                    </div>
                    <?php
                }
                if($this->get_option( 'payment_mode_BTC' ) == "yes"){
                    ?>
                    <div id="OATE_content_BTC" class="OATE_content">
                        TODO: Pendiente
                    </div>
                    <?php
                }
                ?>
                <script>
                    var OATE_LOG = <?=OATE_LOG ? "true":"false"?> ;
                    var OATE_URL = "<?=OATE_URL?>";
                </script>
                <link rel="stylesheet" href="<?=OATE_URL?>src/css/oacquiring-tech-checkout.css?v=<?=OATE_get_version()?>">
                <script src="<?=OATE_URL?>src/js/oacquiring-tech-checkout.js?v=<?=OATE_get_version()?>"></script>
                <?php
		}

		/*
 		 * Fields validation, more in Step 5
		 */
		public function validate_fields() {
            if(!$this->testmode){
                if( empty( $this->get_option( 'merchantId' ) )) {
                    wc_add_notice(  'Oacquiring Tech MerchantId is required!', 'error' );
                    return false;
                }
                if( empty( $this->get_option( 'terminalId' ) )) {
                    wc_add_notice(  'Oacquiring Tech TerminalId is required!', 'error' );
                    return false;
                }
                if( empty( $this->get_option( 'signKey' ) )) {
                    wc_add_notice(  'Oacquiring Tech SignKey is required!', 'error' );
                    return false;
                }
            }
            if( empty( $_POST["OATE_payment_mode"] )) {
                wc_add_notice(  'Payment Mode is required!', 'error' );
                return false;
            }
            switch ($_POST["OATE_payment_mode"]) {
                case 'cc':
                    if( empty( $_POST["OATE_card_number"] )) {
                        wc_add_notice(  'Card Number is required!', 'error' );
                        return false;
                    }
                    $typeCard = OATE_validateCardGetType($_POST["OATE_card_number"]);
                    if($typeCard === false){
                        wc_add_notice(  'Card is invalid!, we only allow Visa and Mastercard', 'error' );
                        return false;
                    }

                    if($typeCard != "Visa" && $typeCard != "Mastercard"){
                        wc_add_notice(  'Card is invalid!, we only allow Visa and Mastercard', 'error' );
                        return false;
                    }

                    if( empty( $_POST["OATE_name"] )) {
                        wc_add_notice(  'Card Name is required!', 'error' );
                        return false;
                    }
                    if( empty( $_POST["OATE_expiry_date"] )) {
                        wc_add_notice(  'Expiry Date is required!', 'error' );
                        return false;
                    }
                    if( empty( $_POST["OATE_cvv"] )) {
                        wc_add_notice(  'CVV is required!', 'error' );
                        return false;
                    }
                    if (!OATE_validateCVV($_POST["OATE_cvv"],$typeCard)) {
                        wc_add_notice(  'CVV is invalid!', 'error' );
                        return false;
                    }
                    break;
                case 'wallet':
                    //TODO: Pendiente
                    wc_add_notice(  'Pendiente', 'error' );
                    return false;
                    break;
                case 'BTC':
                    //TODO: Pendiente
                    wc_add_notice(  'Pendiente', 'error' );
                    return false;
                    break;
                default:
                    wc_add_notice(  'Payment Mode is Invalid!', 'error' );
                    return false;
                    break;
            }
            return true;
		}
        public function generate_checksum($order_id)
        {
            $byEncryp = $this->get_option( 'merchantId' ).$this->get_option( 'terminalId' ).$order_id.$this->get_option( 'signKey' );
            return hash("sha256",$byEncryp);
        }
        public function getApi()
        {
            $api = new API_oacquiring_tech(array(
                "merchantId"    =>  $this->get_option( 'merchantId' ),
                "terminalId"    =>  $this->get_option( 'terminalId' ),
                "signKey"       =>  $this->get_option( 'signKey' ),
                "secret"        =>  $this->get_option( 'secret' ),
                "user"          =>  $this->get_option( 'user' ),
                "password"      =>  $this->get_option( 'password' ),
                "testmode"      =>  $this->testmode,
            ));
            $api->auth();
            return $api;
        }
		/*
		 * We're processing the payments here, everything about it is in Step 5
		 */
		public function process_payment( $order_id ) {
            try {
                global $woocommerce;
    
                $order = wc_get_order( $order_id );
    
                $json = array(
                    "merchant_id"       => $this->get_option( 'merchantId' ),
                    "terminal_id"       => $this->get_option( 'terminalId' ),
                    "redirect_url"      => $order->get_checkout_order_received_url(),
                    "checksum"          => $this->generate_checksum($order_id),
                    "currency"          => strtoupper($order->get_currency()),
                    "payment_mode"      => $_POST["OATE_payment_mode"],
                    "amount"            => $order->get_total(),
                    "order_no"          => $order_id,
                    "notification_url"  => $this->notification_url,
                    "txn_request_url"   => $this->txn_request_url,
                    "txn_request_ip"    => $this->txn_request_ip,
                    "customer_details" => array(
                        "first_name"        => $order->get_billing_first_name(),
                        "last_name"         => $order->get_billing_last_name(),
                        "ip_adress"         => OATE_getIPAddress(),
                        "email"             => $order->get_billing_email(),
                        "phone"             => $order->get_billing_phone(),
                        "zip"               => $order->get_billing_postcode(),
                        "country"           => $order->get_billing_country(),
                        "state"             => $order->get_billing_state(),
                        "city"              => $order->get_billing_city(),
                        "address"           => $order->get_billing_address_1()." ".$order->get_billing_address_2() ,
                    ),
                );
                switch ($_POST["OATE_payment_mode"]) {
                    case 'cc':
                        $typeCard = OATE_validateCardGetType($_POST["OATE_card_number"]);

                        if($typeCard == "Visa"){
                            $json["terminal_id"] = $this->get_option( 'terminalId' );
                        }else if($typeCard == "Mastercard"){
                            $json["terminal_id"] = $this->get_option( 'terminalIdMC' );
                        }else{
                            wc_add_notice(  'Card is invalid!, we only allow Visa and Mastercard', 'error' );
                            return false;
                        }

                        $dateCard = explode("-",$_POST["OATE_expiry_date"]);
                        $json["card_details"] = array(
                            "card_number"       => $_POST["OATE_card_number"],
                            "name"              => $_POST["OATE_name"],
                            "expiry_month"      => $dateCard[1],
                            "expiry_year"       => $dateCard[0],
                            "cvv"               => $_POST["OATE_cvv"],
                        );
                        break;
                    case 'wallet':
                        break;
                    case 'BTC':
                        break;
                }
                $api = $this->getApi();
                $result = $api->sale($json);

                if($result["response_code"] != "R0100"){
                    throw new Exception($result["txn_status_desc"]);
                }

                $order->payment_complete();
                $order->reduce_order_stock();
                $order->add_order_note( 'Hey, your order is paid! Thank you!', true );
                $woocommerce->cart->empty_cart();
    
                update_post_meta(
                    $order_id,
                    "payment",
                    "oacquiring_tech"
                );
                update_post_meta(
                    $order_id,
                    "payment_oacquiring_tech",
                    json_encode($result)
                );
                return array(
                    'result' => 'success',
                    'redirect' => $order->get_checkout_order_received_url()
                );	
            } catch (Exception $error) {
                addOATE_LOG(array(
                    "type"      => "error",
                    "error"     => $error,
                ));
                wc_add_notice(  $error->getMessage(), 'error' );
                return false;
            }
	 	}
 	}
}