<?php

function OATE_refundOrder($order_id)
{
    try {
        $order = wc_get_order( $order_id );
        $payment_oacquiring_tech = get_post_meta($order_id,"payment_oacquiring_tech",true);
        if($payment_oacquiring_tech){
            $payment_oacquiring_tech = json_decode($payment_oacquiring_tech,true);

            $payment = new WC_Oacquiring_Tech_Gateway();

            $api = $payment->getApi();
        
            $result = $api->refund(array(
                "refund_amount"     => $order->get_total(),
                "currency"          => strtoupper($order->get_currency()),
                // "refund_reason"     => "example refund reason her",
                "order_no"          => $order_id,
                "txn_id"            => $payment_oacquiring_tech["txn_id"],
                "checksum"          => $payment->generate_checksum($order_id),
            ));
            
            update_post_meta(
                $order_id,
                "payment_refund_oacquiring_tech",
                json_encode($result)
            );
        }
    } catch (\Throwable $th) {
        addOATE_LOG(array(
            "type"      => "ERROR",
            "error"     => $th,
        ));
    }
}

function OATE_refundOrderHook( $order_id ) { 
    OATE_refundOrder($order_id);
}
add_action( 'woocommerce_order_status_refunded', 'OATE_refundOrderHook', 10, 1 ); 