<?php
if(OATE_LOG){
    function add_OATE_LOG_option_page($admin_bar)
    {
        global $pagenow;
        $admin_bar->add_menu(
            array(
                'id' => 'OATE_LOG',
                'title' => 'OATE_LOG',
                'href' => get_site_url().'/wp-admin/options-general.php?page=OATE_LOG'
            )
        );
    }

    function OATE_LOG_option_page()
    {
        add_options_page(
            'Log OAcquiring Tech',
            'Log OAcquiring Tech',
            'manage_options',
            'OATE_LOG',
            'OATE_LOG_page'
        );
    }

    function OATE_LOG_page()
    {
        $log = OATE_get_optionPage("OATE_LOG");
        $log = array_reverse($log);
        ?>
        <script>
            const log = <?=json_encode($log)?>;
        </script>
        <h1>
            Log de OAcquiring Tech
        </h1>
        <pre><?php var_dump($log);?></pre>
        <?php
    }
    add_action('admin_bar_menu', 'add_OATE_LOG_option_page', 100);

    add_action('admin_menu', 'OATE_LOG_option_page');

}

function addOATE_LOG($newLog)
{
    if(!OATE_LOG){
        return;
    }
    OATE_put_optionPage("OATE_LOG",$newLog);
}