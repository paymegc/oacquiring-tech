<?php

require_once OATE_PATH . "src/function/_index.php";
require_once OATE_PATH . "src/validators/_index.php";
require_once OATE_PATH . "src/hook/_index.php";
require_once OATE_PATH . "src/includes/_index.php";
require_once OATE_PATH . "src/shortcode/_index.php";

require_once OATE_PATH . "src/js/_index.php";
require_once OATE_PATH . "src/sass/_index.php";
require_once OATE_PATH . "src/css/_index.php";
require_once OATE_PATH . "src/img/_index.php";

require_once OATE_PATH . "src/pages/_index.php";
require_once OATE_PATH . "src/log/_index.php";

require_once OATE_PATH . "src/api/_index.php";
require_once OATE_PATH . "src/payment/_index.php";