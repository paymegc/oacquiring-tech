<?php
/*
Plugin Name:OAcquiring Tech
Plugin URI: https://gitlab.com/paymegc/oacquiring-tech
Description: Plugin for Wordpress, used api OAcquiring Tech for generate checkout payment
Author: Francisco Blanco
Version: 1.1.5
Author URI: https://franciscoblanco.vercel.app/
*/

require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'https://gitlab.com/paymegc/oacquiring-tech',
	__FILE__,
	'oacquiring-tech'
);
$myUpdateChecker->setAuthentication('MqiAAZzo9WNBAGzgwc3L');
$myUpdateChecker->setBranch('master');

function OATE_get_version() {
    $plugin_data = get_plugin_data( __FILE__ );
    $plugin_version = $plugin_data['Version'];
    return $plugin_version;
}

define("OATE_LOG",false);
define("OATE_PATH",plugin_dir_path(__FILE__));
define("OATE_URL",plugin_dir_url(__FILE__));

require_once OATE_PATH . "src/_index.php";